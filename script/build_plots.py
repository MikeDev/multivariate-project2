from path import Path
import matplotlib.pyplot as plt
import numpy as np
from imageio import imread

root = Path(__file__).parent.parent
data = root / 'data'
plot = root / 'img'
kaggle_output = data / 'kaggle_output'
gen_imgs = kaggle_output / 'gen_imgs'
rec_imgs = kaggle_output / 'reconstructed'

def loss_plots():

    for file in (data / 'loss_history').files('*.txt'):
        with open(file) as f:
            loss_hs = eval(f.read())
        data = np.matrix(loss_hs)
        mean_epoch = np.mean(data, axis=1)
        plt.plot(np.arange(1, len(mean_epoch)+1), mean_epoch)
        plt.xlabel('epoch')
        plt.ylabel('ELBO')
        plt.title('Training of neural network for ' + file.stem.split('_')[0])
        plt.savefig(plot / file.stem + '_plot.jpg')
        plt.close()

def grid_gen_imgs():
    for dname in ['char', 'fashion']:
        fig, axs = plt.subplots(10, 10)
        fig.suptitle('Evolution of generated images during epochs')
        fig.text(0.05, 0.5, 'Epochs', rotation=90)
        for epoch in range(10, 101, 10):
            for i in range(10):
                i_epoch = int((epoch / 10)-1)
                ax: plt.Axes = axs[i_epoch, i]
                ax.imshow(imread(gen_imgs / f'{dname}_{i}_epoch_{epoch}.jpg'))
                ax.set_xticks([])
                ax.set_yticks([])
                if i == 0:
                    ax.set_ylabel(str(epoch))
                if epoch == 10:
                    ax.set_title(str(i+1))

        plt.savefig(plot / f'{dname}_grid.jpg')
        plt.close()

def grid_reconstructed_imgs():
    fig, axs = plt.subplots(10, 2, figsize=(3, 8))
    for j, dname in enumerate(['char', 'fashion']):
        #fig.suptitle('Reconstructed imgs')
        # fig.text(0.25, 0.05, 'Original')
        # fig.text(0.75, 0.05, 'Reconstructed')
        for i in range(10):
            ax: plt.Axes = axs[i, j]
            ax.imshow(imread(rec_imgs / f'{dname}_{i+1}.png'))
            if j == 0:
                ax.set_ylabel(str(i+1))
            ax.set_xticks([])
            ax.set_yticks([])
            ax.set_frame_on(False)
    plt.savefig(plot / f'rec_grid.jpg')
    plt.close()





if __name__ == '__main__':
    #loss_plots()
    #grid_gen_imgs()
    grid_reconstructed_imgs()